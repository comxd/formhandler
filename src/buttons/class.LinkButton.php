<?php

/**
 * class CancelButton
 *
 * Create a cancel button on the given form
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Buttons
 */
class LinkButton extends CancelButton
{
    public function __construct(\FormHandler $form, $name)
    {
        parent::__construct($form, $name);
    }

    public function getButton()
    {
        $this->setExtra('class="link-button"', true);

        return parent::getButton();
    }
}
