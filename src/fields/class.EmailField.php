<?php

/**
 * class EmailField
 *
 * Create an emailfield
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Fields
 */
class EmailField extends TextField
{
    private $enableViewModeLink = true;

    /**
     * Get view value
     * @return string
     */
    public function _getViewValue()
    {
        $v = $this->getValue();

        if($this->getEnableViewModeLink()
            && trim($v) != '')
        {
            $this->setViewModeLink('mailto:' . $v);
        }
        return parent::_getViewValue();
    }

    /**
     * EmailField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Teye Heimans
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        return sprintf(
                '<input type="email" name="%s" id="%1$s" value="%s" size="%d" %s' . FH_XHTML_CLOSE . '>%s',
                $this->name,
                htmlspecialchars($this->getValue()),
                $this->getSize(),
                (!is_null($this->getMaxLength()) ? 'maxlength="' . $this->getMaxLength() . '" ' : '') .
                    (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '') .
                    (isset($this->extra) ? ' ' . $this->extra . ' ' : '')
                    . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
                (isset($this->extra_after) ? $this->extra_after : '')
        );
    }

    /**
     * Is view mode link enabled?
     *
     * @return boolean
     */
    function getEnableViewModeLink()
    {
        return $this->enableViewModeLink;
    }

    /**
     * Show mailto link on view mode
     *
     * @param type $enableViewModeLink
     * @return static
     */
    function setEnableViewModeLink($enableViewModeLink)
    {
        $this->enableViewModeLink = $enableViewModeLink;
        return $this;
    }
}