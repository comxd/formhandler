<?php

/**
 * class PassField
 *
 * Create a PassField
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */
class PassField extends TextField
{
    private $pre = '';

    /**
     * PassField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Teye Heimans
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return '****';
        }

        return sprintf(
            '%s<input type="password" name="%s" id="%2$s" size="%d" %s' . FH_XHTML_CLOSE . '>%s',
            $this->pre,
            $this->name,
            $this->getSize(),
            (!is_null($this->getMaxLength()) ? 'maxlength="' . $this->getMaxLength() . '" ' : '')
                . (isset($this->tab_index) ? ' tabindex="' . $this->tab_index . '" ' : '')
                . (isset($this->extra) ? $this->extra . ' ' : '')
                . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
            (isset($this->extra_after) ? $this->extra_after : '')
        );
    }

    /**
     * PassField::setPre()
     *
     * Set the message above the passfield
     *
     * @param string $message the message
     * @return PassField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function setPre($message)
    {
        $this->pre = $message;
        return $this;
    }

    /**
     * PassField::checkPassword()
     *
     * Check the value of this field with another passfield
     *
     * @param PassField $object
     * @return boolean true if the values are correct, false if not
     * @author Teye Heimans
     */
    public function checkPassword($object)
    {
        // if the fields doesn't match
        if($this->getValue() != $object->getValue())
        {
            $this->setErrorMessage($this->form_object->_text(15));
            $this->setErrorState(true);
            return;
        }

        // when there is no value
        if($this->getValue() == '' && !$this->form_object->edit)
        {
            $this->setErrorMessage($this->form_object->_text(16));
            $this->setErrorState(true);
            return;
        }
        elseif($this->getValue() == '')
        {
            //in edit mode and value is empty
            return;
        }

        $validator = new Validator();
        // is the password not to short ?
        if(strlen($this->getValue()) < FH_MIN_PASSWORD_LENGTH)
        {
            $this->setErrorMessage(sprintf($this->form_object->_text(17), FH_MIN_PASSWORD_LENGTH));
            $this->setErrorState(true);
            return;
        }
        // is it an valif password ?
        elseif(!$validator->IsPassword($this->getValue()))
        {
            $this->setErrorMessage($this->form_object->_text(18));
            $this->setErrorState(true);
            return;
        }
    }
}