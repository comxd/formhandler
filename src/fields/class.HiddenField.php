<?php
/**
 * class HiddenField
 *
 * Create a hiddenfield on the given form
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */

class HiddenField extends Field
{
    /**
     * Custom definition for a hidden field
     *
     * @param FormHandler $form
     * @param string $name
     * @param callable $validator
     * @return HiddenField
     */
    static function set(FormHandler $form, $name, $validator = null, $foo = null)
    {
        return parent::set($form, '__HIDDEN__', $name, $validator);
    }

    /**
     * Constructor
     *
     * @author Marien den Besten
     * @param FormHandler $form
     * @param string $name
     * @return HiddenField
     */
    public function __construct(FormHandler $form, $name)
    {
        return parent::__construct($form, $name)
            ->setFocusName(null);
    }

    /**
     * HiddenField::getValue();
     *
     * Return the value of the field
     *
     * @return mixed: Value of the field
     */
    public function getValue()
    {
        $value = parent::getValue();

        if(is_string($value) && substr($value,0,11) == '__FH_JSON__')
        {
            $value = json_decode(substr($value,11),true);
        }

        return $value;
    }

    /**
     * HiddenField::getField()
     *
     * Return the HTML of the field
     *
     * @return string: The html of the field
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function getField()
    {
        $value = '';
        if(is_array($this->getValue()))
        {
            $value = '__FH_JSON__'. htmlspecialchars(json_encode($this->getValue()));
        }
        elseif(!is_array($this->getValue()))
        {
            $value = htmlspecialchars($this->getValue());
        }

        return sprintf(
          '<input type="hidden" name="%s" id="%1$s" value="%s" %s'. FH_XHTML_CLOSE .'>%s',
          $this->name,
          $value,
          (isset($this->extra) ? $this->extra.' ' :''),
          (isset($this->extra_after) ? $this->extra_after :'')
        );
    }
}