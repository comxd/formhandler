<?php

/**
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

UploadField::set($form, 'Upload file', 'upload_file')
    ->setDropZoneEnabled(true, 'Drop your RML file here');

TextField::set($form, 'Text field', 'some_other_field')
    ->setValidator(FH_STRING);

$form->_setJS('FormHandler.registerHandlerUploaded(\'upload_file\', function(){ alert(\'File uploaded\'); });', false, true);

SubmitButton::set($form, 'Submit 1');

$form->onCorrect(function($data)
{
    echo '<pre>';
    var_dump($_POST);
    var_dump($data);
    
    if(is_object($data['upload_file']))
    {
        echo "\n". $data['upload_file']->getRealpath();
    }
    
    echo '</pre>';
    return true;
});

$f = $form->flush(true);
echo '<!DOCTYPE html>'
. '<html><head>'
    . '<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>'
    . '<style>'
    . '.FH_dropzone span.upload'
    . '{'
    . 'display:block;'
    . '}'
    . '.FH_dropzone'
    . '{'
    . 'width:250px;'
    . 'padding:30px;'
    . 'display:inline-block;'
    . 'border:3px dashed #CCC;'
    . '}'
    . '.FH_dropzone.dragover'
    . '{'
    . 'border-color:#000;'
    . 'background-color:#EEE;'
    . '}'
    . '</style>'
    . '</head><body>'
    . $f 
    .'</body></html>';