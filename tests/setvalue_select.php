<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */

ob_start();

include '../src/FormHandler.php';

$form = new FormHandler();

$status_list = array(
    'to_be_processed' => 'To be processed',
    'in_process' => 'In process',
    0 => 'Finished',
);

foreach($status_list as $k => $v)
{
    SelectField::set($form, $v, sha1($v))
        ->setDefaultValue($k)
        ->setOptions($status_list);
}

$form->flush();